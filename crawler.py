#爬取WebService地址
import requests
from bs4 import BeautifulSoup
from html.parser import HTMLParser

class WebServiceGenerateTestCase:
    '''
    WebService生成测试用例类
    '''
    def __init__(self):
        self.base_url = 'http://10.36.23.53:8582/'
        self.functionUrls = [] #功能的url，对应.asmx下的所有url
        self.detailUrls = [] #所有具体接口的url

    def getFunctionUrl(self):
        '''
        获取每个功能的url
        '''
        response = requests.get(self.base_url)
        text = BeautifulSoup(response.text,'lxml')
        for item in text.select('pre a'):
            asmxName = str(item['href'])
            if asmxName.endswith('.asmx') == True:
                self.functionUrls.append(self.base_url + item.text)

    def getDetailUrl(self,functionUrl):
        '''
        获取详细地址
        '''
        response = requests.get(functionUrl)
        text = BeautifulSoup(response.text,'lxml')
        for item in text.select('ul li a'):
            self.detailUrls.append(self.base_url + item['href'])

    def parseUrl(self,detailUrl):
        '''
        解析url，获取请求参数和返回值
        '''
        html = HTMLParser()
        # print(detailUrl)
        #region 解析接口名和接口描述
        response = requests.get(detailUrl)
        text = BeautifulSoup(response.text,'lxml')
        apiName = text.select('#content span h2')[0].contents[0] #接口名称
        apiDescripteName = ''
        apiDescripte = text.select('#content span p')[1] #接口描述
        #接口描述可能会为空，可能会有多个，需要特殊处理
        if len(apiDescripte.contents) != 0:
            for content in apiDescripte.contents:
                apiDescripteName += str(content)
        else:
            apiDescripteName = apiName
        print('接口名称={},接口描述={}'.format(apiName , apiDescripteName))
        #endregion

        #region 解析接口请求内容和返回内容
        soap11 = text.select('#content span span')[0] #soap1.1请求参数
        soap11Request = soap11.select('pre')[0]
        soap11RequestStr = str(soap11Request)
        soap11RequestStr = soap11RequestStr.replace('&lt;','<').replace('&gt;','>')
        soap11RequestStr = soap11RequestStr.replace('<font class="value">','').replace('</font>','')
        soap11RequestStr = soap11RequestStr.replace('<pre>','').replace('</pre>','')
        index = soap11RequestStr.find('<?xml version="1.0" encoding="utf-8"?>')
        soap11RequestStr = soap11RequestStr[index:]
        print('请求格式:\r\n' + soap11RequestStr)

        soap11Response = soap11.select('pre')[1]
        soap11ResponseStr = str(soap11Response)
        soap11ResponseStr = soap11ResponseStr.replace('&lt;','<').replace('&gt;','>')
        soap11ResponseStr = soap11ResponseStr.replace('<font class="value">','').replace('</font>','')
        soap11ResponseStr = soap11ResponseStr.replace('<pre>','').replace('</pre>','')
        index = soap11ResponseStr.find('<?xml version="1.0" encoding="utf-8"?>')
        soap11ResponseStr = soap11ResponseStr[index:]
        print('响应格式:\r\n' + soap11ResponseStr)
     
        #endregion

if __name__ == "__main__":
    webService = WebServiceGenerateTestCase()
    webService.getFunctionUrl()
    for funUrl in webService.functionUrls:
        webService.getDetailUrl(funUrl)
    for detailUrl in webService.detailUrls:
        # print(detailUrl)
        webService.parseUrl(detailUrl)
        # break